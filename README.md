c3sets is an attempt to collect the various sets played at c3 events. it is built using hugo.

## contributing

if you want to contribute, either send me your links and i'll add them here as todo (and add the content as soon as i find time) or clone the repository, add the content yourself and send me a patch or merge request.

please use the following template for new entries:
```
---
title: "[<event>] <artist> - <title/description>"
date: <date published>
playdate: <date played>
artists: ["<artist1>","<artist2>", ..]
events: ["<event1>", "<event2>", ..]
genres: ["<genre1>", "<genre2>", ..]
draft: false
---

<link to set>

```

and save it as `content/set/<event>-<artist>-<title/description>.md`.

or - if you have hugo installed - just type `hugo new set/<event>-<artist>-<title/description>.md`. This will add a new pre-filled file you can edit.


## resources

already included:
* https://archive.org/details/CouchsofaLiveSets
* https://www.c-radar.de/2021/01/rc3-lounge-resources/
* https://entropia.de/GPN18:Musik
* https://entropia.de/GPN19:Musik
* https://www.mixcloud.com/matthiasdamasty/
* https://www.mixcloud.com/LoungeControl/
* https://soundcloud.com/das-kraftfuttermischwerk/anti-error-at-32c3
* https://soundcloud.com/tasmo/32c3-antierror-lounge
* https://www.youtube.com/watch?v=lcnTSOICAPk
* https://www.mixcloud.com/shroombab/
* https://soundcloud.com/das-kraftfuttermischwerk
* https://hearthis.at/saetchmo/set/cc-congresse-und-camps/
* https://entropia.de/GPN20:Musik


partial included (only event sets):
* https://hearthis.at/barbnerdy/
* https://soundcloud.com/barbnerdy/tracks
* https://www.mixcloud.com/b4m/
* https://www.mixcloud.com/tasmo/
* https://mixcloud.com/vidister/
* https://soundcloud.com/beh2342/


### todo

* https://git.elektrollart.org/Elektroll/ChaosMusic/src/branch/master/playlist
* https://twitter.com/c3lounge
* https://soundcloud.com/sh1bumi/
* https://www.mixcloud.com/BarbNerdy2/
* https://hearthis.at/tasmo
* https://soundcloud.com/dj-spock-ffm
* https://www.mixcloud.com/dj_spock/
* https://www.mixcloud.com/wando-waiato/
* https://soundcloud.com/beh2342
* https://soundcloud.com/barbnerdy
* https://www.mixcloud.com/wando-waiato/
* https://hearthis.at/mohammed-atari/

33c3:
* https://soundcloud.com/sh1bumi/sets/33c3


rc3:
* https://rc3.protocode.de/ (done for rc3, next up: rc3-2021)
* https://twitter.com/c3lounge/status/1490675479099105286
* https://twitter.com/artfwo/status/1490818242654392323


gregoa:
* https://soundcloud.com/gregoa/grandmaster-g-villa-straylight_1
* https://hearthis.at/gregoa/31c3hall/

gpn21:
* https://www.youtube.com/@LoungeControl

eh20:
* https://cfp.eh20.easterhegg.eu/eh20/schedule/#
* https://www.mixcloud.com/NikTheDusky/eh20-midnight-dnb/

cccamp23:
* https://events.ccc.de/camp/2023/hub/camp23/en/event/dritter-aachen-an-der-saar-aads/
* https://events.ccc.de/camp/2023/hub/camp23/en/event/dj-beh/
* https://www.youtube.com/watch?v=DI6ShBciqyw
* https://soundcloud.com/barbnerdy/mixtape-20-sharing-means-caring-chaos-camp-2023-chill-out-floor

barbnerdy:

* 33c3: https://hearthis.at/barbnerdy/33c3section9barbnerdy2016-chill-out-lounge/
* 35c3: https://hearthis.at/barbnerdy/35c3-c-base-chill-barbnerdy/
* rc3-2021: https://soundcloud.com/barbnerdy/sharing-means-caring-19-dez-2021
* rc3-2021: https://soundcloud.com/barbnerdy/rc3-chillout-2021-remote-chaos-express-experience
* rc3-2021: https://hearthis.at/barbnerdy/01-rc3-chillout-2021-remote-chaos-express-experience/
* camp23: https://hearthis.at/barbnerdy/mixtape-20-sharing-means-caring-chaos-camp-2023-chill-out-floor/
* camp23: https://soundcloud.com/barbnerdy/mixtape-20-sharing-means-caring-chaos-camp-2023-chill-out-floor
* camp19: https://soundcloud.com/barbnerdy/ccamp2019-three-monkeys-stage
* 











