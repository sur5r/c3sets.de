---
title: "[gpn22] MadTiXx"
date: 2024-06-03T07:12:47+02:00
playdate: 2024-06-01T20:00:00+02:00
artists: ['MadTiXx']
events: ['gpn22']
genres: ['indie dance', 'alternative dance', 'techno', 'ebm', 'progressive house']
draft: false
---

https://www.mixcloud.com/madtixx/madtixx-gpn22-gulaschprogrammiernacht-zkm-karlsruhe-gpn22-techno-melodic-indiedance/