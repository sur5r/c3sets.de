---
title: "[rc3-2021] Bleepy Toys - Ambienter Synthesizer Live Jam"
date: 2024-04-06T08:51:19+02:00
playdate: 2021-12-27T21:00:00+01:00
artists: ['Bleepy Toys']
events: ['rc3-2021']
genres: ['ambient techno']
draft: false
---

https://media.ccc.de/v/rc3-2021-sendezentrum-1977-bleepy-toys-a
