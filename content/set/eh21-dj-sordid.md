---
title: "[eh21] DJ Sordid"
date: 2024-04-02T10:43:13+02:00
playdate: 2024-04-01T00:00:00+02:00
artists: ['DJ Sordid']
events: ['eh21']
genres: ['electronic body music', 'ebm', 'industrial', 'rhythm and noise', 'industrial techno']
draft: false
---

https://www.mixcloud.com/sordid/eh21-lounge/
