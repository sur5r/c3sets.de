---
title: "[gpn21] MadTiXx - Progressive House & Melodic Techno"
date: 2024-01-28T13:45:16+01:00
#playdate: 2024-01-28T13:45:16+01:00
artists: ['MadTiXx']
events: ['gpn21']
genres: ['progressive house', 'melodic house', 'indie dance', 'alternative dance', 'techno']
draft: false
---

https://www.mixcloud.com/madtixx/progressive-house-melodic-techno-madtixx-gpn21-lounge-2023-06-08/
