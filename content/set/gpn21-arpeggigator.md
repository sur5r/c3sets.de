---
title: "[gpn21] Arpeggigator"
date: 2024-01-28T13:43:34+01:00
#playdate: 2024-01-28T13:43:34+01:00
artists: ['Arpeggigator']
events: ['gpn21']
genres: ['dawless', 'live electronics', 'dub', 'electro']
draft: false
---

https://www.mixcloud.com/arpeggigator/arpeggigator-gpn21-kuechenstage/
