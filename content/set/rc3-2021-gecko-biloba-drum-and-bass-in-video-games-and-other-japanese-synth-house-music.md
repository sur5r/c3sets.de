---
title: "[rC3-2021] Gecko Biloba - Drum and Bass in Video Games and Other Japanese Synth House Music"
date: 2023-10-11T09:54:25+02:00
playdate: 2021-12-28T21:00:00+02:00
artists: ['gecko']
events: ['rC3-2021']
draft: false
#alternative link: https://archive.org/details/geckosmixtapes/2021-12-28_rc3_japanese-house-and-dnb.mp3
---

https://www.youtube.com/watch?v=mVNipDasqbY
