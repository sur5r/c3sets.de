---
title: "[rC3] gregoa - Valentine Special"
date: 2023-10-13T17:17:18+02:00
playdate: 2020-12-30T12:53:00+02:00
artists: ['gregoa']
events: ['rC3']
draft: false
---

https://hearthis.at/gregoa/b23avalentinespecial01chillinmoods/
https://hearthis.at/gregoa/b23avalentinespecial02fastersounds/
https://hearthis.at/gregoa/b23avalentinespecial03weirdstuff/